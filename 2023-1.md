# 1. Zimski rok 2022/2023, 9.2.2023.

Tablica korisnika:

| Korisnik | Username | Password |  |
| --- | --- | --- | --- |
| Pero Perić | admin | admin | Postavio nekoliko poslova |
| Džuržda Prevodić | trans | asdfasdf.1 | Translator |
| Johnny Bravo | trans2 | asdfasdf.1 | Translator |
| Bender | trans3 | asdfasdf.1 | Translator - ima completed job |

## Zadaci za implementaciju s oba termina: 


### 1. Feature: Link na Admin dashboard (5 bodova)

- Trenutno stanje: link na admin dashboard za superusera ne postoji u navigaciji
- Željeno stanje: link na admin dashboard za superusera je vidljiv u navigaciji
  prije Logout tipke

### 2. Bugfix: User's dashboard - Tekst za prazne kategorije (5 bodova)

- Trenutno stanje: Ukoliko nema Completed Jobs ili My Bids, ostaje naziv kategorije
  ali se ne prikaže ispod ništa
- Željeno stanje: Ukoliko je kategorija prazna (nema ništa za prikazati) potrebno
  je ispisati odgovarajući tekst na engleskom, npr. za My bids treba ispisati:
  "You don't have any bids at the moment." ili nešto slično.


### 3. Feature: User's dashboard - prihvaćeni bid na vrh (10 bodova)

- Trenutno stanje: Bidovi na pojedini job su poredani slučajnim odabirom
- Željeno stanje: Sortirati bidove tako da je prihvaćeni bid prvi, redosljed
  ostalih nije bitan


### 4. Bugfix: Forma za bidanje na job (10 bodova)

- Koraci za rekreiranje buga:
   - Klik na izbor svih jobova
   - Klik na tipku "Submit a quote"
   - Unos cijene i klik na "Submit"
- Ukoliko je korisnik već biddao na taj job, prikaži poruku i sakrij formu
- Na popisu svih jobova: 
   - makni tipku Submit a quote za takve poslove
   - obojaj table border u plavo za takve poslove


### 5. Feature: User's dashboard - My Jobs sekcija (10 bodova)

- Trenutno stanje: korisnik vidi i aktivne i završene poslove u popisu pod My Jobs
- Željeno stanje:
   - korisnik vidi aktivne poslove pod My Active Jobs
   - korisnik vidi obavljene poslove pod My Completed Jobs


### 6. Feature: Omogući slanje poruke (15 bodova)

- Trenutno stanje: Klik na Message Job Owner na All jobs popisu vodi na job_detail
- Željeno stanje: 
   - Klik na Message Job Owner tipku vodi na page za slanje poruke
   - Korisnik može upisati poruku i klikom na Send tipku poslati poruku
   - Poruka je vidljiva u Dashboardu i Sendera i Receivera


### 7. Bugfix: Submit a quote - Translator ne smije biddati veći iznos od budgeta (10 bodova)

- Trenutno stanje: Translator može staviti vrijednost bida koju želi
- Željeno stanje: Translator ne može ponuditi cijenu veću do budgeta


### 8. Bugfix: All jobs - Translator može napraviti Bid na svoj Job (10 bodova)

- Trenutno stanje: Translator u popisu available poslova vidi i svoj job i može 
  biddati na njega
- Željeno stanje:
   - Translator ne smije moći biddati na svoj posao


### 9. Feature: Razdvoji poruke prema senderu (User's Dashboard) (25 bodova)

- Trenutno stanje: korisnik vidi sve poruke koje je dobio sortirane od najnovije prema najstarijoj
- Željeno stanje: 
   - korisnik vidi poruke razdvojene prema pošiljatelju: 
     - U sučelju prvo piše Messages
     - Onda za svakog pošiljatelja piše njegovo ime
     - Onda se vide poruke od tog pošiljatelja
     - Onda ide ime drugog pošiljatelja
     - Onda poruke drugog pošiljatelja
     - itd.
   - Pošiljatelji su sortirani abecedno po imenu uzlazno


### 10. Feature: Messenger app (50 bodova)

- Trenutno stanje: Nije implementirana mogućnost slanja poruka
- Željeno stanje: 
   - Postoji novi app u projektu imena messenger
   - App ima jedan template: messenger.html
   - Sam raspored elemenata je jako sličan Facebook Messengeru
   - Sve rute renderiraju isti template.
   - Kada korisnik otvori messenger, vidi ekran podijeljen u dva stupca:
    širine 2 i 10 kolona
   - U lijevom stupcu su imena korisnika s kojima ima razgovore, klik na nekog 
    od njih otvara u desnom stupcu poruke tog korisnika. Osim toga vidi se 
    30 znakova poslijednje poruke, te kada je poruka poslana.
   - U desnom dijelu na dnu se nalazi input field za pisanje i slanje poruke
   - Iznad su poruke poredane po datumu uzlazno (najnovija poruka je na dnu)
   - Poruke su unutar div-a i mogu se scrollati (overflow: scroll; css property)


